# Assignment: Setting up a Git Repository for an ML Project



## Tasks

### 1. Clone the Starter Repository

    💡 Please, find the Starter Repository in your workspace in the GitLab MLREPA Group.


For this tutorial, we will be using a remote repository named **`project-1-git-assignment-1`**. Please clone this repository to your local machine

### 2. Create a New Branch:  `assignment` 

    💡 It’s important to name the branch name exactly as  `assignment` 
    Other names or typos will break the

Create a new branch named **`assignment`** using the following command:

```bash
git branch -b assignment
```

### 3. Add the  `data/` Directory

In the process of working on your project, you may come across files or folders that are not meant to be included in Git version control, such as temporary files or large data files. These files can clutter your repository and make it more difficult to manage.

To address this, Git uses a **`.gitignore`** file to specify files and directories that should be ignored when committing changes. Let's get started!

#### Sub-task 1: Add the `data/` Directory and Exclude its Contents from Version Control

First, create an empty directory named **`data/`** using the following command:

```bash
mkdir data
```

Next, create a **`.gitignore`** file in the root of your repository:

```bash
touch .gitignore
```

Update the **`.gitignore`** file to exclude the contents of the **`data/`** directory:

```bash
# Add the following line
data/*
```

Commit your updates

```bash
git add .gitignore
git commit -m "Add .gitignore file"

```

Note, at this point Git doesn’t track any files in the `data/` directory. 

Test it with this code. 

```bash
# Git Status command shouldn't show any changes in the repo
touch data/file.txt 
git status
```

Push commits to the remote repository:

```bash
git push origin assignment
```

At this point, Git will not track any files within the **`data/`** directory.

You can test this by creating a new file inside the **`data/`** directory and running the **`git status`** command. The output should not show any changes in the repository.

However, if you check the remote repository on GitLab, you will notice that the **`data/`** directory is not present. This can become problematic if your colleagues clone your repository and expect the **`data/`** directory to be present.

It is a good practice to keep the repository structure consistent between your local and remote repositories. Let's address this issue.

#### Sub-task 2: Make `data/` directory visible in GitLab remote

To make the **`data/`** directory visible in the GitLab remote repository without versioning all files inside it, we can create a hidden file inside the directory and instruct Git to track only that single file.

First, let's create an empty **`.gitignore`** file in the **`data/`** directory:

```bash
touch data/.gitignore
```

Next, add **`data/.gitignore`** to Git by appending the following line to the file:

```bash
echo '!.gitignore' >> data/.gitignore
```

Then, add **`data/.gitignore`** to Git using the force mode:

```bash
git add data/.gitignore -f
```
<details>

  <summary>Explanation</summary>

- The command `git add data/.gitignore -f` is used to forcefully add the `.gitignore` file inside the `data/` directory to the staging area.
- Normally, Git automatically ignores files that are listed in the `.gitignore` file, so they are not included in the next commit. However, using the `-f` flag overrides this behavior and forces Git to include the specified file in the staging area, regardless of its status in the .gitignore file.
- This can be useful in situations where you want to include a specific file in the next commit, even if it matches a pattern listed in the `.gitignore` file.

</details>

Commit and push the updates to the remote repository:

```bash
git commit -m "Add data/.gitignore file"
git push origin assignment
```

By following these steps, you will achieve the following expected results:

- The **`file.txt`** inside the **`data/`** directory will not be added to the Git history.
- The **`data/`** directory will be visible in the GitLab remote repository.

This approach allows you to exclude specific files from version control while still making the directory itself visible in the remote repository. It helps maintain consistency between your local and remote repositories and ensures that your colleagues can access and use the necessary directory structure for your ML project.

### 3. Add `models/` directory

The `models/` directory is used to store trained and serialized models. To add this directory to the repository and track its content with Git, please complete the following tasks:

**TODO:** 

- [ ]  Add the **`models/`** directory to the repository.
- [ ]  Ensure that the content within the **`models/`** directory is tracked by Git.

### 

### 4. Add `reports/` directory

The **`reports/`** directory is intended for storing metrics, generated graphics, and figures used for reporting purposes. To complete this task, follow these steps:

**TODO:** 

- [ ]  Add the **`reports/`** directory to the repository.
- [ ]  Ensure that the content within the **`reports/`** directory is tracked by Git.

### 5. Add `notebooks/` directory

The **`notebooks/`** directory is used for storing Jupyter Notebook files, often utilized for prototyping purposes. To follow best practices, we should exclude these files from being tracked by Git.

**TODO:** 

- [ ]  Add the **`notebooks/`** directory to the repository.
- [ ]  Create a **`.gitignore`** file inside the **`notebooks/`** directory.
- [ ]  Ensure that the content within the **`notebooks/`** directory is **NOT** tracked by Git.

### 6. Add `requirements.txt` file

It is considered good practice to list all the Python dependencies required for the project in a **`requirements.txt`** file. For this template, we will add an empty file.

**TODO:** 

- [ ]  Add an empty **`requirements.txt`** file to the repository.

### 7. Update the `README.md` file (optional)

It is recommended to have a `README.md` file that provides an overview of the repository, including details about its purpose, structure, and instructions for usage.

### 8. Push Updates to the `assignment` Branch on GitLab

```bash
git push origin assignment
```

### 9. Submission: Create a Merge Request to the `main` Branch

Once you have completed all the tasks and are ready to submit your assignment, follow the steps below to create a Merge Request to the **`main`** branch:

1. Go to the GitLab repository page.
2. Click on the "Merge Requests" tab.
3. Click on the "New Merge Request" button.
4. Set the source branch to **`assignment`** and the target branch to **`main`**.
5. Provide a title and description for your Merge Request.
6. Review the changes and make sure everything looks correct.
7. Click on the "Submit Merge Request" button.

⚠️ Please **DO NOT MERGE** your request until you receive a submission report attached to your Merge Request. 

- Creating a merge request will trigger a CI pipeline that automatically checks our work.
- A test report will be generated and added to your merge request.
- This report will contain the results of the automated tests and checks on your assignment.
- After passing all tests, you may proceed with merging the updates into the **`main`** branch.

Good luck with your submission! 🙌🏻

## Requirements

To ensure the successful completion of the assignment, please make sure the following requirements are met:

- The source branch name for the Merge Request is **`assignment`**.
- The **`data/`** directory in GitLab only contains a **`.gitignore`** file  (files added here are **NOT** tracked by Git)
- The **`models/`** directory in GitLab has an empty **`.gitignore`** file (files added here are tracked by Git)
- The **`reports/`** directory in GitLab has an empty **`.gitignore`** (files added here are tracked by Git)
- The **`notebooks/`** directory in GitLab only contains a **`.gitignore`** (files added here are **NOT** tracked by Git)
- The README.md file has been updated to provide an overview of the repository, including details about its purpose, structure, and usage instructions.

# Pro Tips

**1. Use and share the template**

Congratulations on completing the assignment! You have successfully created a Git Repo Template that can be used for new projects. Simply fork or clone the template, rename it, and start using it in your future project

**1. Use VSCode IDE to generate `.gitignore` files**

VSCode IDE provides a handy option to generate  `.gitignore` file for Python language: 

- press `CTRL + Shift + P` (`CMD + Shift + P` on macOS) to open the command palette.
- type in `Add gitignore` in the command palette.
- Update code & commit